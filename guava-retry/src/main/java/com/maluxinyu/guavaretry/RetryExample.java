package com.maluxinyu.guavaretry;

import java.time.Duration;

import com.google.common.util.concurrent.RateLimiter;

import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;

public class RetryExample {

    public static void main(String[] args) {
        // 创建重试配置
        RetryConfig config = RetryConfig.<Boolean>custom()
                .maxAttempts(3) // 最大重试次数
                .waitDuration(Duration.ofMillis(1000)) // 重试间隔时间
                .build();
        // 创建 Retryer
        Retry retryer = Retry.of("my-retry", config);

        // 定义需要重试的操作
        Retry.decorateRunnable(retryer, () -> {
            // 模拟一个可能会失败的操作
            if (RateLimiter.create(0.5).tryAcquire()) {
                System.out.println("操作成功");
            } else {
                throw new RuntimeException("操作失败");
            }
        }).run();
    }
}
