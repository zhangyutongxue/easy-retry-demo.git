package com.maluxinyu.easyretry.dao;

import org.springframework.stereotype.Repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.maluxinyu.easyretry.po.FailOrderPo;

@Repository
public interface FailOrderBaseMapper extends BaseMapper<FailOrderPo> {

}
