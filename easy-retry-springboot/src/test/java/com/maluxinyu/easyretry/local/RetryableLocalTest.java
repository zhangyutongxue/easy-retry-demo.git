package com.maluxinyu.easyretry.local;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.maluxinyu.easyretry.service.LocalRetryService;

import lombok.extern.slf4j.Slf4j;

@SpringBootTest
@Slf4j
public class RetryableLocalTest {

    @Autowired
    private LocalRetryService localRetryService;

//    @Test
//    public void localRetryTest(){
//        localRetryService.localRetry();
//    }
//
//    @Test
//    public void localRetryWithBasicParamsTest(){
//        localRetryService.localRetryWithBasicParams();
//    }
//
//    @Test
//    public void localRetryIncludeExceptionTest(){
//        localRetryService.localRetryIncludeException();
//    }
//
//    @Test
//    public void localRetryExcludeExceptionTest(){
//        localRetryService.localRetryExcludeException();
//    }
//
//    @Test
//    public void localRetryIsThrowExceptionTest(){localRetryService.localRetryIsThrowException();}
}
