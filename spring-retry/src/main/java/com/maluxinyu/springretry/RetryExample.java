package com.maluxinyu.springretry;

import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.retry.annotation.Retryable;

@EnableRetry
public class RetryExample {

    @Retryable(value = RuntimeException.class, maxAttempts = 3, backoff = @Backoff(delay = 1000))
    public void performOperation() {
        // 模拟一个可能会失败的操作
        if (Math.random() < 0.5) {
            System.out.println("操作成功");
        } else {
            throw new RuntimeException("操作失败");
        }
    }

    public static void main(String[] args) {
        RetryExample example = new RetryExample();
        example.performOperation();
    }
}
